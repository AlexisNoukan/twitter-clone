/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["cdn-icons-png.flaticon.com", "links.papareact.com"],
  },
}

module.exports = nextConfig
