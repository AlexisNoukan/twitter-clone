import React, { SVGProps } from "react"

interface Props {
  Icon: (props: SVGProps<SVGSVGElement>) => JSX.Element
  text: string
}

function SidebarRow({ Icon, text }: Props) {
  return (
    <div className="flex items-center max-w-fit space-x-2 px-4 py-3 rounded-full hover:bg-gray-100 transition-all duration-200 group">
      <Icon className="h-6 w-6" />
      <p className="text-base font-light lg:text-xl hidden md:inline-flex group-hover:text-twitter">
        {text}
      </p>
    </div>
  )
}

export default SidebarRow
