import React from "react"
import Image from "next/image"
import SidebarRow from "./SidebarRow"
import {
  HomeIcon,
  HashtagIcon,
  BellIcon,
  BookmarkIcon,
  MailIcon,
  UserIcon,
  CollectionIcon,
  DotsCircleHorizontalIcon,
} from "@heroicons/react/outline"

export default function Sidebar() {
  return (
    <div className="col-span-2 flex flex-col items-center md:items-start">
      {/*Twitter Logo */}
      <Image
        alt="twitter logo"
        width="50"
        height="50"
        className="m-3"
        src="https://cdn-icons-png.flaticon.com/512/1384/1384065.png"
      ></Image>
      {/* Menu */}
      <div className="flex flex-col items-center md:items-start">
        <SidebarRow text="Home" Icon={HomeIcon} />
        <SidebarRow text="Explore" Icon={HashtagIcon} />
        <SidebarRow text="Notifications" Icon={BellIcon} />
        <SidebarRow text="Messages" Icon={MailIcon} />
        <SidebarRow text="Boockmarks" Icon={BookmarkIcon} />
        <SidebarRow text="Lists" Icon={CollectionIcon} />
        <SidebarRow text="Sign In" Icon={UserIcon} />

        <SidebarRow text="More" Icon={DotsCircleHorizontalIcon} />
      </div>
      {/* Button */}
      {/* Profile */}
    </div>
  )
}
