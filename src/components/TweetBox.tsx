import {
  CalendarIcon,
  EmojiHappyIcon,
  LocationMarkerIcon,
  SearchCircleIcon,
  PhotographIcon,
} from "@heroicons/react/outline"
import React, { useState } from "react"

export default function TweetBox() {
  const [input, setInput] = useState<string>("")
  return (
    <div className="flex space-x-2 p-5">
      <img
        className="mt-4 h-14 w-14 object-cover rounded-full"
        src="https://links.papareact.com/gll"
        alt="profile picture"
      />
      <div className="flex flex-1 items-center pl-2">
        <form className="flex flex-1 flex-col">
          <input
            className="h-24 w-full text-xl outline-none placeholder:text-xl"
            type="text"
            value={input}
            onChange={(e) => setInput(e.target.value)}
            placeholder="What's happening ?"
          />
          <div className="flex items-center">
            <div className="flex space-x-2 text-twitter flex-1">
              {/* Icon */}
              <PhotographIcon className="w-5 h-5 cursor-pointer transition-transform duration-150 ease-out hover:scale-150" />
              <SearchCircleIcon className="w-5 h-5 cursor-pointer transition-transform duration-150 ease-out hover:scale-150" />
              <EmojiHappyIcon className="w-5 h-5 cursor-pointer transition-transform duration-150 ease-out hover:scale-150" />
              <CalendarIcon className="w-5 h-5 cursor-pointer transition-transform duration-150 ease-out hover:scale-150" />
              <LocationMarkerIcon className="w-5 h-5 cursor-pointer transition-transform duration-150 ease-out hover:scale-150" />
            </div>
            <button
              disabled={!input}
              className="bg-twitter text-white rounded-full px-5 py-2 font-bold disabled:opacity-40"
            >
              Tweet
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}
